#!/usr/bin/python

import sys
import pathlib
import concurrent.futures
from shutil import copyfile
from pyicloud import PyiCloudService

api = api = PyiCloudService('icloud@user.com', '<password>')

if api.requires_2sa:
    import click
    print("Two-step authentication required. Your trusted devices are:")

    devices = api.trusted_devices
    for i, device in enumerate(devices):
        print("  %s: %s" % (i, device.get('deviceName',
            "SMS to %s" % device.get('phoneNumber'))))

    device = click.prompt('Which device would you like to use?', default=0)
    device = devices[device]
    if not api.send_verification_code(device):
        print("Failed to send verification code")
        sys.exit(1)

    code = click.prompt('Please enter validation code')
    if not api.validate_verification_code(device, code):
        print("Failed to verify verification code")
        sys.exit(1)


def download_file(photo, timeout):
    create_date = photo.created
    year = create_date.strftime('%Y')
    month = create_date.strftime('%m')
    filename = "/Photos/%s/%s/%s" % (year, month, photo.filename.rstrip())
    path = pathlib.Path(filename)
    path.parent.mkdir(parents=True, exist_ok=True)
    download = photo.download()
    with open(filename, 'wb') as opened_file:
        opened_file.write(download.raw.read())

    return "%s downloaded" % filename

# We can use a with statement to ensure threads are cleaned up promptly
with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
    # Start the load operations and mark each future with its URL
    future_to_url = {executor.submit(download_file, photo, 60): photo for photo in api.photos.all}

